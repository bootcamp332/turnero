# Generated by Django 4.1.7 on 2023-02-19 19:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Boxes',
            fields=[
                ('box_id', models.AutoField(primary_key=True, serialize=False)),
                ('descripcion', models.CharField(max_length=100, unique=True)),
                ('abreviatura', models.CharField(max_length=10, unique=True)),
                ('esta_activo', models.BooleanField()),
            ],
            options={
                'db_table': 'boxes',
            },
        ),
        migrations.CreateModel(
            name='DjangoMigrations',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('app', models.CharField(max_length=255)),
                ('name', models.CharField(max_length=255)),
                ('applied', models.DateTimeField()),
            ],
            options={
                'db_table': 'django_migrations',
            },
        ),
        migrations.CreateModel(
            name='Estados',
            fields=[
                ('estado_id', models.IntegerField(primary_key=True, serialize=False)),
                ('cod_estado', models.CharField(max_length=10, unique=True)),
                ('descripcion', models.CharField(max_length=50, unique=True)),
                ('esta_activo', models.BooleanField()),
            ],
            options={
                'db_table': 'estados',
            },
        ),
        migrations.CreateModel(
            name='Prioridades',
            fields=[
                ('prioridad_id', models.IntegerField(primary_key=True, serialize=False)),
                ('descripcion', models.CharField(max_length=50, unique=True)),
                ('prioridad', models.IntegerField()),
            ],
            options={
                'db_table': 'prioridades',
            },
        ),
        migrations.CreateModel(
            name='Servicios',
            fields=[
                ('servicio_id', models.AutoField(primary_key=True, serialize=False)),
                ('descripcion', models.CharField(max_length=50, unique=True)),
                ('abreviatura', models.CharField(max_length=10, unique=True)),
                ('esta_activo', models.BooleanField()),
            ],
            options={
                'db_table': 'servicios',
            },
        ),
        migrations.CreateModel(
            name='Socios',
            fields=[
                ('socio_id', models.IntegerField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=100)),
                ('nro_documento', models.CharField(max_length=20, unique=True)),
            ],
            options={
                'db_table': 'socios',
            },
        ),
        migrations.CreateModel(
            name='Tickets',
            fields=[
                ('ticket_id', models.AutoField(primary_key=True, serialize=False)),
                ('cod_ticket', models.CharField(blank=True, max_length=20, null=True, unique=True)),
                ('fecha_hora_ingreso_registro', models.DateTimeField(blank=True, null=True)),
                ('nro_documento', models.CharField(max_length=20)),
                ('prioridad', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.prioridades')),
            ],
            options={
                'db_table': 'tickets',
            },
        ),
        migrations.CreateModel(
            name='Usuarios',
            fields=[
                ('usuario_id', models.AutoField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=100, unique=True)),
                ('login', models.CharField(max_length=50, unique=True)),
                ('clave_de_acceso', models.CharField(max_length=100)),
                ('es_administrador', models.BooleanField()),
                ('esta_activo', models.BooleanField()),
            ],
            options={
                'db_table': 'usuarios',
            },
        ),
        migrations.CreateModel(
            name='BoxesServicios',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('box', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.boxes')),
                ('servicio', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.servicios')),
            ],
            options={
                'db_table': 'boxes_servicios',
            },
        ),
        migrations.CreateModel(
            name='SiguienteNroServicio',
            fields=[
                ('servicio', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='app.servicios')),
                ('fecha', models.DateField()),
                ('nro_siguiente', models.IntegerField()),
            ],
            options={
                'db_table': 'siguiente_nro_servicio',
                'unique_together': {('servicio', 'fecha')},
            },
        ),
        migrations.CreateModel(
            name='TicketsSocios',
            fields=[
                ('ticket', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='app.tickets')),
                ('socio', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.socios')),
            ],
            options={
                'db_table': 'tickets_socios',
                'unique_together': {('ticket', 'socio')},
            },
        ),
        migrations.CreateModel(
            name='TicketsServicios',
            fields=[
                ('ticket', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='app.tickets')),
                ('item', models.IntegerField()),
                ('fecha_hora_ingreso_registro', models.DateTimeField()),
                ('fecha_hora_inicio_atencion', models.DateTimeField(blank=True, null=True)),
                ('fecha_hora_fin_atencion', models.DateTimeField(blank=True, null=True)),
                ('box_id_atencion', models.ForeignKey(blank=True, db_column='box_id_atencion', null=True, on_delete=django.db.models.deletion.CASCADE, to='app.boxes')),
                ('estado', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.estados')),
                ('servicio', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.servicios')),
            ],
            options={
                'db_table': 'tickets_servicios',
                'unique_together': {('ticket', 'item')},
            },
        ),
        migrations.CreateModel(
            name='AperturaCierreBoxes',
            fields=[
                ('box', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='app.boxes')),
                ('fecha_hora_apertura', models.DateTimeField()),
                ('fecha_hora_cierre', models.DateTimeField(blank=True, null=True)),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.usuarios')),
            ],
            options={
                'db_table': 'apertura_cierre_boxes',
                'unique_together': {('box', 'fecha_hora_apertura')},
            },
        ),
    ]
