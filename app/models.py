# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Boxes(models.Model):
    box_id = models.AutoField(primary_key=True)
    descripcion = models.CharField(unique=True, max_length=100)
    abreviatura = models.CharField(unique=True, max_length=10)
    esta_activo = models.BooleanField()

    class Meta:
        # managed = False
        db_table = 'boxes'


class Servicios(models.Model):
    servicio_id = models.AutoField(primary_key=True)
    descripcion = models.CharField(unique=True, max_length=50)
    abreviatura = models.CharField(unique=True, max_length=10)
    esta_activo = models.BooleanField()

    class Meta:
        # managed = False
        db_table = 'servicios'


class BoxesServicios(models.Model):
    box = models.ForeignKey(Boxes, on_delete=models.CASCADE)
    servicio = models.ForeignKey('Servicios', on_delete=models.CASCADE)

    class Meta:
        # managed = False
        db_table = 'boxes_servicios'


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        # managed = False
        db_table = 'django_migrations'


class Estados(models.Model):
    estado_id = models.IntegerField(primary_key=True)
    cod_estado = models.CharField(unique=True, max_length=10)
    descripcion = models.CharField(unique=True, max_length=50)
    esta_activo = models.BooleanField()

    class Meta:
        # managed = False
        db_table = 'estados'


class Prioridades(models.Model):
    prioridad_id = models.IntegerField(primary_key=True)
    descripcion = models.CharField(unique=True, max_length=50)
    prioridad = models.IntegerField()

    class Meta:
        # managed = False
        db_table = 'prioridades'


class SiguienteNroServicio(models.Model):
    servicio = models.OneToOneField(Servicios, on_delete=models.CASCADE, primary_key=True)
    fecha = models.DateField()
    nro_siguiente = models.IntegerField()

    class Meta:
        # managed = False
        db_table = 'siguiente_nro_servicio'
        unique_together = (('servicio', 'fecha'),)


class Socios(models.Model):
    socio_id = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=100)
    nro_documento = models.CharField(unique=True, max_length=20)

    class Meta:
        # managed = False
        db_table = 'socios'


class Tickets(models.Model):
    ticket_id = models.AutoField(primary_key=True)
    cod_ticket = models.CharField(unique=True, max_length=20, blank=True, null=True)
    fecha_hora_ingreso_registro = models.DateTimeField(blank=True, null=True)
    nro_documento = models.CharField(max_length=20)
    prioridad = models.ForeignKey(Prioridades, on_delete=models.CASCADE)

    class Meta:
        # managed = False
        db_table = 'tickets'


class TicketsServicios(models.Model):
    ticket = models.OneToOneField(Tickets, on_delete=models.CASCADE, primary_key=True)
    item = models.IntegerField()
    servicio = models.ForeignKey(Servicios, on_delete=models.CASCADE)
    fecha_hora_ingreso_registro = models.DateTimeField()
    fecha_hora_inicio_atencion = models.DateTimeField(blank=True, null=True)
    fecha_hora_fin_atencion = models.DateTimeField(blank=True, null=True)
    estado = models.ForeignKey(Estados, on_delete=models.CASCADE)
    box_id_atencion = models.ForeignKey(Boxes, on_delete=models.CASCADE, db_column='box_id_atencion', blank=True, null=True)

    class Meta:
        # managed = False
        db_table = 'tickets_servicios'
        unique_together = (('ticket', 'item'),)


class TicketsSocios(models.Model):
    ticket = models.OneToOneField(Tickets, on_delete=models.CASCADE, primary_key=True)
    socio = models.ForeignKey(Socios, on_delete=models.CASCADE)

    class Meta:
        # managed = False
        db_table = 'tickets_socios'
        unique_together = (('ticket', 'socio'),)


class Usuarios(models.Model):
    usuario_id = models.AutoField(primary_key=True)
    nombre = models.CharField(unique=True, max_length=100)
    login = models.CharField(unique=True, max_length=50)
    clave_de_acceso = models.CharField(max_length=100)
    es_administrador = models.BooleanField()
    esta_activo = models.BooleanField()

    class Meta:
        # managed = False
        db_table = 'usuarios'


class AperturaCierreBoxes(models.Model):
    box = models.OneToOneField('Boxes', on_delete=models.CASCADE, primary_key=True)
    fecha_hora_apertura = models.DateTimeField()
    fecha_hora_cierre = models.DateTimeField(blank=True, null=True)
    usuario = models.ForeignKey('Usuarios', on_delete=models.CASCADE)

    class Meta:
        # managed = False
        db_table = 'apertura_cierre_boxes'
        unique_together = (('box', 'fecha_hora_apertura'),)